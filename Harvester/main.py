import nmap
from scapy.all import ARP, Ether, srp
import socket 
import tkinter as tk 
import sys 
import json
import os
from datetime import datetime
from git import Repo
import shutil
from customtkinter import *
import customtkinter

output_folder = "scan_results"


repo_path = r'D:\BACHELOR3\MSPR 6.1\MSPR_6.1\MSPR6-1-WEB'   # Chemin vers repo gitlab, à modifier en fonction du chemin du repo cloné

def add_file_to_gitlab_repository(file_path, branch_name, commit_message):
    
    repo = Repo(repo_path)  # défini le repo Git

    
    shutil.copy(file_path, repo_path)  # Copie du fichier JSON vers le répértoire du repo

    
    repo.git.add('.')   # Ajoute le fichier JSON dans le repo

    
    repo.index.commit(commit_message)   # Effectue un commit avec un message

    
    origin = repo.remote(name='origin')   # Pousse les modifications vers le GitLab
    origin.push(branch_name)

    
def scanvm_interface():# Fonction pour créer et afficher l'interface pour le scan VM
    global result_text, file_path  # Déclarer result_text comme globale

    
    def scanvm():   # Fonction pour créer et afficher l'interface pour le scan VM
        begin = int(port_start_entry.get())
        end = int(port_end_entry.get())
        target = ip_entry.get()
        L = []  # Liste pour les ports ouverts
        F = []  # Liste pour les ports fermés ou filtrés
        scanner = nmap.PortScanner() 
        for i in range(begin, end + 1): 
            res = scanner.scan(target, str(i)) 
            res = res['scan'][target]['tcp'][i]['state'] 
            if res == "open":
                L.append({"port": i, "state": res})
            else:
                F.append({"port": i, "state": res})
            result_text.insert(tk.END, f'port {i} is {res}.\n') 

        
        scan_result = {"IP": target, "open_ports": L, "closed_or_filtered_ports": F}  # Ajout de l'adresse IP au dictionnaire

        
        global file_path  # Écris les résultats dans un fichier JSON
        timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
        file_path = os.path.join(output_folder, f"SCPM_{timestamp}.json")
        with open(os.path.join(output_folder, f"SCPM_{timestamp}.json"), "w") as f:
            json.dump(scan_result, f, indent=4)
        
        
        branch_name = 'main'   # Appel de la fonction pour ajouter et pousser le fichier JSON au dépôt GitLab
        commit_message = f"Ajout du fichier JSON: {file_path}"
        add_file_to_gitlab_repository(file_path, branch_name, commit_message)

    
    vm_window = tk.Toplevel(window)  # Création de la fenêtre pour le scan VM
    vm_window.title("Scan VM")
    vm_window.geometry("400x600")
    vm_window.config(background="#525760")

    
    ip_label = tk.Label(vm_window,    # Champ de saisie pour l'adresse IP de la machine
                        text="IP ADDRESS: ",
                        font=("Helvetica", 12),
                        bg='#525760',
                        fg="#fff")
    ip_label.pack(pady=11)
    ip_entry = CTkEntry(vm_window,
                        width=150,
                        height=30,
                        placeholder_text='IP' )
    ip_entry.pack()

    
    port_start_label = tk.Label(vm_window,   # Champs de saisie pour la plage de ports à scanner
                                text="START :",
                                font=("Helvetica", 12),
                                bg='#525760',
                                fg="#fff")
    port_start_label.pack(pady=11)
    port_start_entry = CTkEntry(vm_window,
                                width=150,
                                height=30,
                                placeholder_text='START PORT N°')
    port_start_entry.pack()

    port_end_label = tk.Label(vm_window,
                              text="END :",
                              font=("Helvetica", 12),
                              bg='#525760',
                              fg="#fff")
    port_end_label.pack(pady=11)
    port_end_entry = CTkEntry(vm_window,
                              placeholder_text='END PORT N°',
                              width=150,
                              height=30)
    port_end_entry.pack()

    
    scan_button = CTkButton(vm_window,  # Bouton pour lancer le scan
                            text="SCAN",
                            command=scanvm,
                            font=("Helvetica", 15),
                            fg_color="#3E537A",
                            border_color="#fff",
                            border_width=1,
                            corner_radius=15,
                            text_color="#fff",
                            hover_color="#9CC2CD",)
    scan_button.pack(pady=15)

    
    result_text = tk.Text(vm_window,   # Zone de texte pour afficher les résultats
                          height=10,
                          width=100,
                          bg='#333D4F',
                          fg="#fff"
                          )
    result_text.pack(pady=1)

def scan_network_interface():
    
    def scan_devices_on_network():  # Fonction pour créer et afficher l'interface pour le scan réseau
        ip_range = ip_range_entry.get()
        arp = ARP(pdst=ip_range)
        ether = Ether(dst="ff:ff:ff:ff:ff:ff")
        packet = ether/arp
        result = srp(packet, timeout=3, verbose=0)[0]
        devices = []
        for sent, received in result:
            ip_address = received.psrc
            hostname = get_hostname(ip_address)
            if hostname != "N/A":
                devices.append({"hostname": hostname, "ip": ip_address})
                result_text.insert(tk.END, f"Device: {hostname} - IP: {ip_address}\n")
        timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
        file_path = os.path.join(output_folder, f"SCNW_{timestamp}.json")
        with open(file_path, "w") as f:
            json.dump(devices, f, indent=4)
        
        
        branch_name = 'main'   # Appel de la fonction pour ajouter et pousser le fichier JSON au dépôt GitLab
        commit_message = f"Ajout du fichier JSON: {file_path}"
        add_file_to_gitlab_repository(file_path, branch_name, commit_message)

    
    network_window = tk.Toplevel(window)   # Création de la fenêtre pour le scan réseau
    network_window.title("Scan Network")
    network_window.geometry("400x600")
    network_window.config(background="#525760")

    
    ip_range_label = tk.Label(network_window,   # Champ de saisie pour la plage d'IP à scanner
                              text="ENTER IP RANGE :",
                              font=("Helvetica",12),
                              fg="#fff",
                              bg="#525760")
    ip_range_label.pack(pady=11)
    ip_range_entry = CTkEntry(network_window,
                              placeholder_text='IP/MASK',
                              width=150,
                              height=30)
    ip_range_entry.pack(pady=11)

    
    scan_button = CTkButton(network_window,  # Bouton pour lancer le scan
                            text="SCAN",
                            command=scan_devices_on_network,
                            font=("Helvetica", 15),
                            fg_color="#3E537A",
                            border_color="#fff",
                            border_width=1,
                            corner_radius=15,
                            text_color="#fff",
                            hover_color="#9CC2CD",)
    scan_button.pack(pady=15)

   
    result_text = tk.Text(network_window,   # Zone de texte pour afficher les résultats
                          height=10,
                          width=50,
                          bg='#333D4F',
                          fg="#fff")
    result_text.pack()

def get_hostname(ip_address):
    try:
        hostname, _, _ = socket.gethostbyaddr(ip_address)
        return hostname
    except (socket.herror, socket.gaierror, socket.timeout):
        return "N/A"
    
def exit_run(): 
    '''fonction pour quitter l'application'''
    window.quit()
    sys.exit()



window = tk.Tk()  # Création de la fenêtre principale
window.title("Harvester")
window.geometry("550x550")
window.config(background='#525760')


bienvenue_label = tk.Label(window, text="Welcome to Harvester", font=("Helvetica", 25),fg="#fff",bg="#525760")  # Titre "Bienvenue"
bienvenue_label.place(relx=0.5, rely=0.2, anchor="center")  # Centrer le titre horizontalement


boutons_cadre = tk.Frame(window,bg="#525760")   # Cadre pour contenir les boutons
boutons_cadre.place(relx=0.5, rely=0.5, anchor="center")  # Centrer le cadre des boutons horizontalement et verticalement


scan_vm_button = CTkButton(boutons_cadre,  # Bouton pour ouvrir l'interface de scan VM
                           bg_color="#525760",
                           fg_color="#3E537A",
                           text_color="#fff",
                           hover_color="#9CC2CD",
                           text="Scan Port/Machine",
                           command=scanvm_interface,
                           height=40,width=50,
                           font=("Helvetica", 18),
                           border_width=1.5,
                           corner_radius=15)
scan_vm_button.grid(row=0, column=0, padx=10)  # Utilisation de padx pour espacer les boutons


scan_network_button = CTkButton(boutons_cadre,  # Bouton pour ouvrir l'interface de scan réseau
                                bg_color="#525760",
                                fg_color="#3E537A",
                                text_color="#fff",
                                text="Scan Network",
                                command=scan_network_interface,
                                hover_color="#9CC2CD",
                                height=40,width=50,
                                corner_radius=15,
                                font=("Helvetica", 18),
                                border_width=1.5)
scan_network_button.grid(row=0, column=1, padx=10)


quitter_button = CTkButton(window, # Bouton "Quitter"
                           fg_color="#3E537A",
                           text_color="#fff",
                           text="Quitter",
                           command=exit_run,
                           width=80,
                           hover_color="#9CC2CD",
                           corner_radius=15,
                           font=("Helvetica", 12),
                           border_width=1.5)
quitter_button.grid(row=2, column=10, padx=10, pady=10)

if not os.path.exists(output_folder):
    os.makedirs(output_folder)

window.grid_rowconfigure(1, weight=1)

window.mainloop()
