<?php
session_start();
if(isset($_POST['logout'])) {
    // Détruire toutes les données de session
    session_destroy();

    // Rediriger vers la page de connexion ou une autre page après la déconnexion
    header('Location: connexion.php');
    exit;
}
?>
</html>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styleindex.css">
    <title>Nester</title>
</head>
    <header>
        <form method="post" action="">
            <input type="submit" name="logout" value="Déconnexion">
        </form>
        <p class="connecter">Connecté en tant que : <?php $username = $_SESSION['username']; echo "{$username}"; ?></p>
        <h1>MSPR6.1</h1>
        <nav>
            <ul>
                <li><a href="parcours.php">Parcours</a></li>
                <!--<li><a href="#">A propos</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Contact</a></li>
                -->
            </ul>
        </nav>       
    </header>
<body>

    <section class="main-content">
        <h2>Tableau nmap</h2>
        <p class="titre">Ici s'afficheront les données de nmap dans un tableau </p>
    </section>

    <?php

// Remplacez l'URL du fichier JSON sur GitLab par le vôtre
$json_url = 'https://gitlab.com/SteeveParis/MSPR6-1-WEB/-/raw/main/SCNW.json';

// Récupérer les données JSON depuis GitLab
$json_data = file_get_contents($json_url);

// Décoder les données JSON
$data = json_decode($json_data, true);

// Vérifier si le décodage a réussi
if ($data !== null) {
    // Commencer la construction du tableau
    echo '<table id="nmap" border="1">';
    
    // En-tête du tableau
    echo '<tr><th>Hostname</th><th>IP</th></tr>';
    
    // Boucle à travers les données pour construire les lignes du tableau
    foreach ($data as $item) {
        echo '<tr>';
        echo '<td>' . htmlspecialchars($item['hostname']) . '</td>';
        echo '<td>' . htmlspecialchars($item['ip']) . '</td>';
        echo '</tr>';
    }
    
    // Fin du tableau
    echo '</table>';
} else {
    // En cas d'échec du décodage JSON
    echo 'Erreur lors de la récupération des données depuis GitLab.';
}

?>



</body>
    <footer>
        <p>&copy; 2024 MSPR6.1. Tous droits reserves.</p>
    </footer>
</html>