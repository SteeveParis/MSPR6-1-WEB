<?php
session_start();
if(isset($_POST['logout'])) {
    // Détruire toutes les données de session
    session_destroy();

    // Rediriger vers la page de connexion ou une autre page après la déconnexion
    header('Location: connexion.php');
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="styleindex.css">
    <title>Tableaux JSON</title>
</head>

<header>
        <form method="post" action="">
            <input type="submit" name="logout" value="Déconnexion">
        </form>
        <p class="connecter">Connecté en tant que : <?php $username = $_SESSION['username']; echo "{$username}"; ?></p>
        <h1>MSPR6.1</h1>
        <nav>
            <ul>
                <li><a href="index2.php">Accueil</a></li>
            </ul>
        </nav>       
    </header>
	
<body>
<?php

// URL du fichier JSON brut dans le dépôt GitLab
$gitlab_raw_url = 'https://gitlab.com/SteeveParis/MSPR6-1-WEB/-/raw/main/SCPM.json';

// Récupérer le contenu JSON depuis le fichier brut
$json_data = file_get_contents($gitlab_raw_url);

// Convertir le JSON en tableau PHP
$data = json_decode($json_data, true);

// Vérifier si le décodage a réussi
if ($data !== null) {
    // Afficher le tableau HTML
    echo '<table>';
    echo '<tr><th>IP</th><th>Port</th><th>État</th></tr>';
    
    foreach ($data['closed_or_filtered_ports'] as $port) {
        echo '<tr>';
        echo '<td>' . htmlspecialchars($data['IP']) . '</td>';
        echo '<td>' . htmlspecialchars($port['port']) . '</td>';
        echo '<td>' . htmlspecialchars($port['state']) . '</td>';
        echo '</tr>';
    }
    
    echo '</table>';
} else {
    echo "Erreur lors de la récupération du fichier JSON depuis GitLab.";
}
?>




</body>
	<footer>
        <p>&copy; 2024 MSPR6.1. Tous droits reserves.</p>
    </footer>
</html>
